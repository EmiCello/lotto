import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;


public class LottoDemo {

    public static void main(String[] args) throws MalformedURLException, IOException {

        URL url = new URL("http://www.mbnet.com.pl/dl.txt");
        File file = new File("C:\\Users\\Emilia\\Desktop\\downloaded");

        LottoHistoryDownloader lottoHistoryDownloader = new LottoHistoryDownloader();
        File downloadedFile = lottoHistoryDownloader.download(url, file);

        Map<Integer, Integer> resultMap = new TreeMap<>();

        List<String[]> l = prepareList(downloadedFile);

        for (String[] s : l) {
            for (String a : s) {
                System.out.print(a + " ");
            }
        }

        for (String[] s : l) {
            for (String value : s) {
                int number = Integer.parseInt(value);
                resultMap.put(number, resultMap.getOrDefault(number, 0) + 1);
            }

        }

        System.out.println(" ");

        for(int i = 1; i < 50; i++){
            System.out.println("Wyniki dla liczby " + i + " to " + resultMap.get(i));
        }



       Map<Integer, Integer> newMapAfterSorted = resultMap.entrySet().stream()
                                                    .sorted(Comparator.comparing(e -> e.getValue(), Comparator.reverseOrder()))
                                                    .limit(6)
                                                    .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));
        System.out.println("_____________________");

        newMapAfterSorted.forEach((key, value) -> System.out.println("Wyniki dla liczby " + key + " to " + value));

    }


    public static List<String[]> prepareList(File downloadedFile) throws IOException {

        List<String[]> listaTablic = new ArrayList<>();

        String[] list = new String[5];
        BufferedReader br = new BufferedReader(new FileReader(downloadedFile));
        String line;
        while ((line = br.readLine()) != null) {
            String numbers = line.substring(line.lastIndexOf(" ")).trim();
            list = numbers.split(",");
            listaTablic.add(list);
        }
        return listaTablic;
    }

}