import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.FileUtils;


public class FileDownloadResponseHandler implements ResponseHandler<File>{

    private final File target;

    public FileDownloadResponseHandler(File target) {
        this.target = target;
    }

    @Override
    public File handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode < 200 || statusCode >= 300){
            return null;
        }
        InputStream source = response.getEntity().getContent();
        FileUtils.copyInputStreamToFile(source, this.target);
        return this.target;
    }
}
