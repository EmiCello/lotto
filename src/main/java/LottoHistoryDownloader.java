import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import java.io.File;
import java.net.URL;

public class LottoHistoryDownloader {

    public File download(URL url, File file) {

        CloseableHttpClient httpclient = HttpClients.custom().build();

        File downloaded = null;

        try {
            HttpGet get = new HttpGet(url.toURI());
            downloaded = httpclient.execute(get, new FileDownloadResponseHandler(file));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return downloaded;
    }


}
